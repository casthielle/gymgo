import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';


/**
 * Generated class for the DayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-day',
  templateUrl: 'day.html',
})
export class DayPage {

  day = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.day = this.navParams.get('day');
    this.day.exercises = [{
      muscle: "Pectorales",
      routines: [{
        name: "Press de banca",
        repeat: 15,
        series: 3,
        breakTime: 60
      },{
        name: "Apertura",
        repeat: 15,
        series: 3,
        breakTime: 60
      }]
    },{
      muscle: "Biceps",
      routines: [{
        name: "Press Neutro",
        repeat: 15,
        series: 3,
        breakTime: 60
      },{
        name: "Press Frances",
        repeat: 15,
        series: 3,
        breakTime: 60
      }]
    }];
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
