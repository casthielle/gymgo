import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the TrainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-train',
  templateUrl: 'train.html',
})
export class TrainPage {

  routine = {
    day: "Lunes",
    exercises: [{
        exercise: "Press de banca",
        progress: 20,
      },{
        exercise: "Press de banca",
        progress: 50,
      },{
        exercise: "Press de banca",
        progress: 60,
    }]
  };

  indicator = {
    max: 100,
    color: "#ffbe2e",
    background: "#ececec",
    rounded: true,
    radius: 50,
    stroke: 10,
    responsive: true,
    animationDelay: 500
  };

    // [duration]="800"
    // [animation]="'easeInOutQuart'"
    // [animationDelay]="0"

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainPage');
  }

}
