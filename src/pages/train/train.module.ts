import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainPage } from './train';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

@NgModule({
  declarations: [
    TrainPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainPage),
    RoundProgressModule
  ],
})
export class TrainPageModule {}
