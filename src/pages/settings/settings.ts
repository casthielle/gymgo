import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RoutinePage } from '../routine/routine';
import { ScannerPage } from '../scanner/scanner';


/**
 * Generated class for the BeginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  settings = {
    height: "",
    weight: "",
    birthdate: "",
    qrcode: ""
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
  ) {
    this.settings.qrcode = this.navParams.get('qrcode');
  }

  goToRoutine() {
    this.navCtrl.push(RoutinePage);
  }

  goToScanner() {
    this.navCtrl.push(ScannerPage);
  }
}
