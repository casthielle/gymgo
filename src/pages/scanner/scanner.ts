import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { QRScanner } from '@ionic-native/qr-scanner';
import { SettingsPage } from '../settings/settings';

/**
 * Generated class for the ScannerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-scanner',
  templateUrl: 'scanner.html',
})
export class ScannerPage {

  data = {
    code: "",
    img: "assets/imgs/scanner-bg.svg"
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private qrScanner: QRScanner,
    public toastCtrl: ToastController
  ) {
    this._scan();
  }

  _toast(){
    let toast = this.toastCtrl.create({
      message: 'Codigo escaneado correctamente.',
      duration: 5000,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: "OK",
      cssClass: "custom-toast"
    });
    toast.present();
  }

  send(){
    this.navCtrl.push(SettingsPage, { qrcode: this.data.code });
  }

  refresh() {
    this.data.code = "";
    this.data.img = "assets/imgs/scanner-bg.svg";
    this._scan();
  }

  _scan(){
    let scanSub = this.qrScanner.scan().subscribe((text: string) => {
      this.data.code = text;
      this.data.img = "assets/imgs/scanner-bg-green.svg";
      this._toast();
      this.qrScanner.hide(); // hide camera preview
      scanSub.unsubscribe(); // stop scanning
    });
    this.qrScanner.show();
  }

}
