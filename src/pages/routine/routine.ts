import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';

import { DayPage } from '../day/day';
import { HomePage } from '../home/home';

/**
 * Generated class for the RoutinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-routine',
  templateUrl: 'routine.html',
})
export class RoutinePage {

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }

  congifure(day){
    let profileModal = this.modalCtrl.create(DayPage, { day: day });
    profileModal.present();
  }

  goToHome(){
    this.navCtrl.push(HomePage);
  }

  days: Array<any> = [{
      id: 0,
      name: "Lunes"
    }, {
      id: 1,
      name: "Martes"
    }, {
      id: 2,
      name: "Miercoles"
    }, {
      id: 3,
      name: "Jueves"
    }, {
      id: 4,
      name: "Viernes"
    }, {
      id: 5,
      name: "Sabado"
  }]

}
