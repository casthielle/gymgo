import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { QRScanner } from '@ionic-native/qr-scanner';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { InitPage } from '../pages/init/init';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { SettingsPage } from '../pages/settings/settings';
import { RoutinePage } from '../pages/routine/routine';
import { ScannerPage } from '../pages/scanner/scanner';
import { DayPage } from '../pages/day/day';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    InitPage,
    LoginPage,
    RegisterPage,
    SettingsPage,
    RoutinePage,
    ScannerPage,
    DayPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    InitPage,
    LoginPage,
    RegisterPage,
    SettingsPage,
    RoutinePage,
    ScannerPage,
    DayPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    QRScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
