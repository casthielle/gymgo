import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';

@Component({
  selector: 'page-init',
  templateUrl: 'init.html'
})

export class InitPage {

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private qrScanner: QRScanner
  ) {
    this._getPermissions();
  }

  _getPermissions() {
    this.qrScanner.prepare().then((status: QRScannerStatus) => {
      if (status.authorized) {

      }
      else if (status.denied) {
         // camera permission was permanently denied
         // you must use QRScanner.openSettings() method to guide the user to the settings page
         // then they can grant the permission from there
      }
      else {
         // permission was denied, but not permanently. You can ask for permission again at a later time.
      }
    }).catch((e: any) => console.log('Error is', e));
  }

  goToLogin() {
    this.navCtrl.push(LoginPage);
  }

  goToRegister() {
    this.navCtrl.push(RegisterPage);
  }
}
